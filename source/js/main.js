'use strict';
//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

// галерея
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

// анимация SVG
//= ../node_modules/vivus/src/pathformer.js
//= ../node_modules/vivus/src/vivus.js


//= library/carousel.js
//= library/wow.js
//= library/jquery-ui.js


$(document).ready(function () {


	/*
	// 
	$.post( "test.php", $( "form.calc" ).serialize(), function (data) {
		// ответ от сервера
	}, "json" );
	*/



	/* анимация блоков */
	new WOW().init({
		mobile: false
	});


	/* START preloader*/
	(function () {
		if ( Date.now() - preloader.timerStart >= preloader.duration  ) {
			$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
				$("#preloader-cube").css("display", "none");
			});
		} else {
			setTimeout(function () {
				$("#preloader-cube").animate({"opacity" : "0"}, preloader.duration / 2, function () {
					$("#preloader-cube").css("display", "none");
				});
			}, preloader.duration / 1.7)
		}
	})();
	/* END preloader */

	/* START Открытие меню */
	$(".burger").on("click", function () {
		$(this).toggleClass("active");
		var height = $(".header").innerHeight();
		$(".navigation__mobile").css( "top", height );

		if ( $(this).hasClass("active") ) {
			$(".navigation__mobile").addClass("active");
		} else {
			$(".navigation__mobile").removeClass("active");
		}

		if ( $(window).width() < 991 ) {
			$(".header").addClass("bg-dark");
		}
	});
	/* END откртие меню*/




	/* START красиво для hover в navigation */
		$(".navigation__item").click(function() {
			$(".navigation__item").removeClass('active');
			$(this).addClass("active");
		});	
	/* END красиво для hover в navigation */





	if ( $(window).scrollTop() > 0 ) {
		//$("header").addClass("bg-dark");
	}
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			$(".header").addClass("bg-dark");
		}  else {
			if ( $(".navigation__mobile").hasClass("active") ) {

			} else {
				$(".header").removeClass("bg-dark");
			}
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 400);
		return false;
	});
	/* END кнопка вверх */




	// SVG animation
	var welcome_svg = new Vivus(
		'welcome__line',
		{
			type: 'oneByOne',
			duration: 100
		}, function () {
			$('#welcome__line').animate({'opacity': 0}, 500);
			$('.welcome__bg').animate({'opacity': 1}, 500);
			$('.navigation').animate({'opacity': 1}, 500);
			$('.welcome__content').animate({'opacity': 1}, 500);
		});
	new Vivus(
		'home__svg',
		{
			type: 'oneByOne',
			duration: 70
	}, function () {
		$(".svg__img").animate({'opacity': 1}, 500);
	});








	// Якоря
	$('a[href*="#"]').click(function() {
		var top = $(".header").innerHeight();
		console.log(top);
		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top - top
		}, 400);
		
		$(".navigation__mobile").removeClass("active");
		$(".burger").removeClass("active");
		$("#burger").removeClass("active");

		return false;
	});

});


